
# OpenMP

## Slides

We are going to follow Professor Chow's notes on OpenMP [1](https://www.cc.gatech.edu/~echow/ipcc/hpc-course/02_openmp.pdf) and [2](https://www.cc.gatech.edu/~echow/ipcc/hpc-course/04_openmp.pdf) while
going through the examples in this directory.

## Quick Reference

### [Directives](https://computing.llnl.gov/tutorials/openMP/#Directives)

Things that can follow `#pragma omp` in C/C++ or `!$OMP` in Fortran:

* [`parallel`](https://computing.llnl.gov/tutorials/openMP/#ParallelRegion)
* [`for`](https://computing.llnl.gov/tutorials/openMP/#DO)
* [`ordered`](https://computing.llnl.gov/tutorials/openMP/#ORDERED)
    * Called within a `for` loop with `ordered` clause
* [`sections`](https://computing.llnl.gov/tutorials/openMP/#ORDERED)
    * Contains independent `section` directives
* [`single`](https://computing.llnl.gov/tutorials/openMP/#SINGLE)
* [`task`](https://computing.llnl.gov/tutorials/openMP/#Task)
* [`barrier`](https://computing.llnl.gov/tutorials/openMP/#BARRIER)
* [`critical`](https://computing.llnl.gov/tutorials/openMP/#CRITICAL)
* [`atomic`](https://computing.llnl.gov/tutorials/openMP/#ATOMIC)

### Data Scope Clauses

* [`private`](https://computing.llnl.gov/tutorials/openMP/#PRIVATE)
* [`firstprivate`](https://computing.llnl.gov/tutorials/openMP/#FIRSTPRIVATE)
* [`lastprivate`](https://computing.llnl.gov/tutorials/openMP/#LASTPRIVATE)
* [`reduction`](https://computing.llnl.gov/tutorials/openMP/#REDUCTION)

### Important [Environment Variables](https://computing.llnl.gov/tutorials/openMP/#EnvironmentVariables)

* `OMP_NUM_THREADS`
* `OMP_SCHEDULE`
* `OMP_DISPLAY_ENV`
* `OMP_PROC_BIND`
* [`OMP_PLACES`](https://gcc.gnu.org/onlinedocs/libgomp/OMP_005fPLACES.html) (or `GOMP_CPU_AFFINITY` (GNU), `KMP_AFFINITY` (Intel))


## OpenMP Performance

[EPCC](https://www.epcc.ed.ac.uk/research/computing/performance-characterisation-and-benchmarking/epcc-openmp-micro-benchmark-suite) maintains a micro-benchmark suite for measuring the overhead of OpenMP constructs.

## Regarding `schedule(dynamic)`

Look at the code for `openmp-ex17`: it runs a loop of size 16 with 2 threads
and `schedule(dynamic)` and prints out, in order, the iteration assignments.
  
Our two competing hypotheses were that, given $N$ workers with $o$ occupied
and $M$ remaining iterations, the next chunk size would be either $M/N$ or
$M/(N-o)$.  Before running the example, think about what you think the output
would be for each case.

I think the output makes it clear that the correct chunk size is $M/N$.  The
actual algorithm is given in the [API Specification](http://www.openmp.org/wp-content/uploads/openmp-4.5.pdf)
on page 61.

## Regarding `ordered`

The new example `openmp-ex14b` makes it easier to understand scheduling because
it prints out each iteration *in order*.  To achieve this, an `ordered` clause
is added to the loop, followed by an `ordered` directive specifying the region
that should be executed sequentially in order.

## Additional Reading

* Hager & Wellein, Chapters 6 & 7

* [Professor Vuduc's](http://vuduc.org/cse6230/slides/cse6230-fa14--04-omp.pdf) slides

* ["Introduction to Shared-memory parallel processing with OpenMP" (Wellein, Hager & Wittmann](https://moodle.rrze.uni-erlangen.de/pluginfile.php/13116/mod_resource/content/4/08_06_07-2017-PTfS.pdf)

* [OpenMP 4.5 Reference Card](http://www.openmp.org/wp-content/uploads/OpenMP-4.5-1115-CPP-web.pdf)

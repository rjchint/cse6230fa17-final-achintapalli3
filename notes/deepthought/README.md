# Deepthought

See the [College of Computing Deepthought Page][deepthought]

## Slurm commands

`Slurm` is the job scheduler on Deepthought: it controls access to the compute nodes.

[Here][slurmcheatsheet] is a good cheat sheet on Slurm commands.

* Start an interactive job: `srun-pclass--pty/bin/bash`

* Batch script format:

    #!/bin/sh
    #SBATCH --job-name=my_important_job   # Job name
    #SBATCH --mail-type=ALL               # Mail events (NONE, BEGIN, END, FAIL, ALL)
    #SBATCH --mail-user=<email_address>   # Where to send mail	
    #SBATCH --time=00:05:00               # Time limit hrs:min:sec
    #SBATCH --output=my_imp_job_%j.out    # Standard output and error log

		pwd; hostname; date

    ./my_important_program

		date;

* Submit a batch job: `sbatch my-important-job.sh`

* What's running, what's waiting? `squeue [-u me]`

* What's my job's status? `sinfo <jobid>`

* Wait, stop, stop, stop! `scancel <jobid>`


## Additional Reading

[deepthought]: https://support.cc.gatech.edu/facilities/instructional-labs/deepthought-cluster "The Deepthought Cluster"

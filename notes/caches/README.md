# Cache Operations

## The cache hierarchy

* **Note** Although I sometimes omit them in diagrams, the
  [*registers*](https://en.wikipedia.org/wiki/Processor_register) -- the
  scratch space to/from which CPU calculations are written/read are sometimes
  thought of as the closest cache to CPU.  But I don't like this thinking,
  because every time memory is **load**ed from a register it is to an explicit
  register location.  Also, the data that was in that register is not
  automatically evicted to cache: the result of a complete operations must be
  **store**d.

* sizes `lscpu | grep cache`:

		L1d cache:             32K
    L1i cache:             32K
    L2 cache:              256K
    L3 cache:              3072K

    * `L1i` is instructions, `L1d` is data
    * Not listed: main memory, whose size varies much more system-to-system: my laptop has 8GB, "fat" nodes may be hundreds of GB or even TB.

* latencies: here a "cycles for fastest load-to-use" for [Kaby Lake](https://en.wikichip.org/wiki/intel/microarchitectures/kaby_lake):

    * L1: 4 cycles
    * L2: 12 cycles
    * L3: 42 cycles
    * Mem: 42 cycles + 51 ns

    * This translates to ~1 ns latency for L1, ~100 ns latency for main memory
    * See [NLPE (slide 72)][NLPE]

* bandwidths: covered in depth in [exercise 01](../exercises/01-Introduction-STREAM/)

## Cache Read Policies

* The amount of data moved from one level of cache to the next is a *cache
  line* (CL): typically 64B = 8W, exploiting spatial locality

### [Associativity](https://en.wikipedia.org/wiki/CPU_cache#Associativity)

### Prefetching

* There are [software hints]() (the compiled program includes prefetching
  instructions) and [hardware prefetchers]() (hardware that tries to detect a
  stream and start prefetching). See [Lee, Kim, & Vuduc](http://doi.acm.org/10.1145/2133382.2133384)
* Further exploit spatial locality

## Cache Write Policies

### [Write-Through](https://en.wikipedia.org/wiki/Cache_(computing)#WRITE-THROUGH) vs. [Write-Back](https://en.wikipedia.org/wiki/Cache_(computing)#WRITE-BACK)

### [Nontemporal Stores](https://blogs.fau.de/hager/archives/2103)

## Multiprocessor Setting: Cache Coherence

* Remember that memory latency is stagnant or growing from STREAM lecture: [Slide ("Why is Memory Latency Stagnant or growing?")](https://sites.utexas.edu/jdm4372/2016/11/22/sc16-invited-talk-memory-bandwidth-and-system-balance-in-hpc-systems/)

    * One source of latency is the need to keep all copies of cache *coherent*:
      if one processor writes data to memory, other processors shouldn't use an
      invalidated version of that memory that is currently in their cache.

* [Snooping](https://en.wikipedia.org/wiki/Bus_snooping)

## Additional Reading

* ["Node Level Performance Engineering" (Hager & Wellein)][NLPE]

[NLPE]: https://moodle.rrze.uni-erlangen.de/pluginfile.php/12220/mod_resource/content/10/01_Arch.pdf "Node Level Performance Engineering (Hager & Wellein)" 

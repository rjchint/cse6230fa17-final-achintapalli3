# CSE 6230: High Performance Parallel Computing

## Introduction

The goals of this class are to be able to

* *design*,
* *implement*, and
* *analyze* high-performance code.

Implementation through practical example will be the focus of this course, but
the three aspects complement each other.

## From the [Syllabus](../../README.md)

I want you to be able to:

* *Design* high-performance code.  Good design requires good algorithms for
  the problem at hand -- the subject of other courses like [CSE 6220: Intro to
  HPC](http://cse6220.gatech.edu/sp17-oms/) -- but also good understanding of
  the computer system being used in the form of *performance models*.  This
  course will discuss practical performance modeling of current architectures,
  including multi-core, coprocessor (e.g. GPUs) and many-core designs.

---

* *Implement* high-performance code, drawing on a wide range software
  engineering tools, including libraries, language extensions, intrinsics, and
  compiler directives.  This course will have coding assignments that will
  familiarize you with what these tools can -- and can't -- do.

---

* *Analyze* high-performance code. This may require more sophisticated
  measurement than just the runtime on a given problem.  For performance to be
  reproducible and transferable, we must understand *why* it is or isn't fast.
  This course will present tools for measuring the behavior of different
  components of a computer system to identify bottlenecks and confirm whether
  the *implementation* matches the *design*.

## Logistics

(This is where I talked about a lot of stuff that was on the
[syllabus](../../README.md).)

# Where is HPC in CSE?

## The Continuum of Problem Spaces

Computational science and engineering can be thought of as the study of taking
problems from very *abstract* application definitions to very *concrete*
computation performed by a computer.

![Continuum of problem spaces](img/continuum.jpg)

---

* *Computing Models* turn abstract problems into algorithms

* *Languages, Libraries, \& Extensions* turn algorithms into code

* *Compilers* turn code into programs

* *Operating Systems \& Runtimes* turn programs into computation

---

![Continuum of problem spaces](img/continuum.jpg)

* [CSE 6220](http://cse6220.gatech.edu/sp17-oms/) covers
  Abstract Problems -> Algorithms with parallel models

* This class focuses on Algorithms -> Code -> Programs

* Operating systems are covered in [CS 3120](https://oscar.gatech.edu/pls/bprod/bwckctlg.p_disp_course_detail?cat_term_in=201708&subj_code_in=CS&crse_numb_in=3210)

* Hardware design in [CS 6290](http://www.omscs.gatech.edu/cs-6290-high-performance-computer-architecture/)

## Abstract: Applications & Problem Definitions

Examples of *applications*, which I think of as problem definitions whose
problems can be stated without reference to algorithms used to solve them or
the systems on which they are solved:

* Solving equations:
    * Ordinary differential (ODEs)
    * Partial differential (PDEs)
    * Stochastic \[partial\](SDEs/SPDEs)
    * (recommended to me after class) integral equations

* Simulating molecular dynamics (e.g. protein folding)

* Neural Network (outputs / training)

* [Markov Chain] Monte Carlo simulations (computing expectations &
  probabilities)

* Graph analysis

## Computing Models

Comptuting models (or "abstract machines") like this allow us to convert
applications in algorithms expressed in terms of their operations.


So an application ("Solve this ODE") is converted into an algorithm
($Y \leftarrow F(X)$), whose performance in terms of the abstract machine can be
analyzed (e.g. time complexity $T_F(X)$ or space complexity $S_F(X)$)

---

A few models:

* *Sequential Models*:
    * [RAM (Random-Access Machine) Model](https://en.wikipedia.org/wiki/Random-access_machine)
    * [Idealized Cache Model](https://en.wikipedia.org/wiki/Cache-oblivious_algorithm#Idealized_cache_model)

* *Shared Memory Models*: [PRAM (Parallel RAM) Model](https://en.wikipedia.org/wiki/Parallel_random-access_machine) in three flavors:
    * Concurrent Read Concurrent Write (CRCW)
    * Concurrent Read Exclusive Write (CREW)
    * Exclusive Read Exclusive Write (EREW)

* *Distributed Memory (Network) Models*:
    * [BSP (Bulk Synchronous Parallel) Model](https://en.wikipedia.org/wiki/Bulk_synchronous_parallel)
    * [LogP Model](https://en.wikipedia.org/wiki/LogP_machine)

* *Task Parallelism Models*:
    * [Circuit Models](https://en.wikipedia.org/wiki/Circuit_(computer_science))

## Even Simple Models (RAM) Are Useful For Designing High-Performance Code

Example of solving Poisson's equation (slide from David Keyes): improvements in
numerical algorithms (no reference to parallelism) can lead to big performance gains.

![speedup per year as new algorithms developed](img/keyespoisson.jpg){height=2in}

## Nevertheless, a sequential model is not accurate

* [Out-of-order execution](https://en.wikipedia.org/wiki/Out-of-order_execution)
  became common in the 1990s
* [MMX](https://en.wikipedia.org/wiki/MMX_(instruction_set)) vector instructions
  debuted in 1997
* Multiple cores became common in the 2000s

![Microprocessor trends ([Horowitz et al. via C. Batten, updated by K. Rupp](https://www.karlrupp.net/2015/06/40-years-of-microprocessor-trend-data/))](img/40-years-processor-trend.png){height=2in}

## Concrete: Hardware & Computation

We survey some of the forms of parallelism in modern hardware

---

### Ports to multiple execution units in a CPU

![Ports to execution units that can execute simultaneously ([hardwaresecrets.com](http://www.hardwaresecrets.com/inside-intel-core-microarchitecture/4/))](img/ports.gif)

---

### Vector processing units & vectorized registers and instructions: single instruction, multiple data (SIMD)

![Vector addition in an AVX-512 VPU ([Intel, codeproject.com](https://www.codeproject.com/Articles/1182515/Vectorization-Opportunities-for-Improved-Perform))](img/avx512.png)

---

### Threading

![[ecnmag.com](https://www.ecnmag.com/article/2012/05/real-time-apps-using-multi-threads)](img/smt.jpg){height=2in}

Top: one thread occupies whole CPU, switching when a stall occurs.  Bottom: different threads using different execution units simultaneously.

---

### Multiple Cores per Processor

* A *core* is a CPU with its exclusive cache hierarchy (typically L1 & L2)
* *shared* main memory

![[intel.com](https://software.intel.com/en-us/articles/intel-performance-counter-monitor)](img/multicoresystem.png)

---

### Coprocessing devices

![NVidia Pascal block diagram [nvidia.com](https://devblogs.nvidia.com/parallelforall/inside-pascal/)](img/pascal.png)

---

### Interconnected Nodes

![Sunway TaihuLight Supercomputer [insidehpc.com](http://insidehpc.com/wp-content/uploads/2016/06/sunway-1.jpg)](img/sunway.jpg)

## Additional Reading

* Hager & Wellein, Chapter 1

* ["Node Level Performance Engineering" (Hager & Wellein)](https://moodle.rrze.uni-erlangen.de/pluginfile.php/12220/mod_resource/content/10/01_Arch.pdf), in particular:
    * Slides 41--45 discuss the intruction level parallelism (that they also
      call "superscalarity") from the multiple execution units in a single CPU
    * Slides 47--58 discuss single instruction multiple data parallelism from
      vectorized operations
    * Slides 59--61 show all of the factors that go into wringing FLOPs out of
      a CPU cyle: ILP * SIMD * (Fused multiply add instruction)
    * Slide 103 is a good diagram of the different terminologies: node, socket,
      core, processor.  One distinction I make that isn't on this slide is
      that, with hardware multithreading, a physical core will be treated as
      multiple processors (for example, in `proc/cpuinfo`).


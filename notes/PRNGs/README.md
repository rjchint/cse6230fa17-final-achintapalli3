# Pseudorandom Number Generators

* `rand()` may not be thread-safe, but in modern implementations it often is.
    * Thread safety is typically implementated with *locks* on global state.
        * Are the locks scalable?  How quickly can they handle lots of simultaneous requests?  See `randtest.c`
    * May have a better implementation than the original standard, or may not.
* `rand_r()` is thread-safe.
    * The 32 bits of state in `seed` do not allow for changing to a higher-quality implementation behind the scenes.

* `drand48()` and `erand48()` have specific (linear congruential) generators
  with 48-bits of state (3 `shorts`) and two parameters `a` and `c`
    * `erand48()` is reentrant and thread-safe (provided no other thread changes `a` and `c`
    * 48-bits is better than 32, but not by much
    * Bad behavior: correlation between consecutive numbers in a sequence /
      between sequences started with consecutive seeds

* Longer recurrence PRNGs ([Mersenne
  Twister](https://en.wikipedia.org/wiki/Mersenne_Twister) better emulate true
  randomness (MT is behind `<random>` in C++), but are more expensive to
  compute, have much larger state (MT state is 2.5 KiB): 
## Additional Reading

* Great article about high-quality parallel pseudorandom number generator developed at DE Shaw: ["Parallel random numbers: As easy as 1, 2, 3"][random123]

[random123]: https://doi.org/10.1145/2063384.2063405 "Parallel random numbers: As easy as 1, 2, 3 (Salmon, Moraes et al., 2011)"

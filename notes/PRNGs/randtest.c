#include <stdio.h>
#include <stdlib.h>
#include <tictoc.h>
#include <cse6230rand.h>
#include <float.h>
#include <math.h>
#if defined(_OPENMP)
#include <omp.h>
#endif

#define NPRNGS 6

int main(void)
{
  int         N = 10000000;
  int         NTIMES = 10;
  int         i;
  TicTocTimer timer;
  double      min_time[NPRNGS] = {DBL_MAX,DBL_MAX,DBL_MAX,DBL_MAX,DBL_MAX,DBL_MAX},
              max_time[NPRNGS] = {0.}, mean_time[NPRNGS] = {0.};
  double      cse6230_mean = 0., cse6230_var = 0.;
  double      cse6230n_mean = 0., cse6230n_var = 0.;

  for (i = 0; i < NTIMES; i++) {
    double global_mean, global_var, time;

    timer = tic();
    global_mean = 0.;
    #pragma omp parallel
    {
      int    j;
      double my_mean = 0.;
      #pragma omp for
      for (j = 0; j < N; j++) {
        my_mean += (double) rand() / RAND_MAX;
      }
      #pragma omp atomic
      global_mean += my_mean;
    }
    global_mean /= N;
    time = toc(&timer);
    if (i) {
      min_time[0] = time < min_time[0] ? time : min_time[0];
      max_time[0] = time > max_time[0] ? time : max_time[0];
      mean_time[0] += time;
    }

    timer = tic();
    global_mean = 0.;
    #pragma omp parallel
    {
      int          j;
      double       my_mean = 0.;
      unsigned int seed = 0;
#if defined(_OPENMP)
      seed = omp_get_thread_num();
#endif
      #pragma omp for
      for (j = 0; j < N; j++) {
        my_mean += (double) rand_r(&seed) / RAND_MAX;
      }
      #pragma omp atomic
      global_mean += my_mean;
    }
    global_mean /= N;
    time = toc(&timer);
    if (i) {
      min_time[1] = time < min_time[1] ? time : min_time[1];
      max_time[1] = time > max_time[1] ? time : max_time[1];
      mean_time[1] += time;
    }

    timer = tic();
    global_mean = 0.;
    #pragma omp parallel
    {
      int    j;
      double my_mean = 0.;
      #pragma omp for
      for (j = 0; j < N; j++) {
        my_mean += drand48();
      }
      #pragma omp atomic
      global_mean += my_mean;
    }
    global_mean /= N;
    time = toc(&timer);
    if (i) {
      min_time[2] = time < min_time[2] ? time : min_time[2];
      max_time[2] = time > max_time[2] ? time : max_time[2];
      mean_time[2] += time;
    }

    timer = tic();
    global_mean = 0.;
    #pragma omp parallel
    {
      int    j;
      double my_mean = 0.;
      unsigned short seed[3] = {0};
#if defined(_OPENMP)
      seed[0] = omp_get_thread_num();
#endif
      #pragma omp for
      for (j = 0; j < N; j++) {
        my_mean += erand48(seed);
      }
      #pragma omp atomic
      global_mean += my_mean;
    }
    global_mean /= N;
    time = toc(&timer);
    if (i) {
      min_time[3] = time < min_time[3] ? time : min_time[3];
      max_time[3] = time > max_time[3] ? time : max_time[3];
      mean_time[3] += time;
    }

    timer = tic();
    global_mean = 0.;
    global_var = 0.;
    #pragma omp parallel
    {
      int           j, seed = 0;
      double        my_mean = 0.;
      double        my_sqmean = 0.;
      cse6230rand_t urand;
#if defined(_OPENMP)
      seed = omp_get_thread_num();
#endif
      cse6230rand_seed(seed * NTIMES + i,&urand);
      #pragma omp for
      for (j = 0; j < N; j++) {
        double rval = cse6230rand(&urand);
        my_mean += rval;
        my_sqmean += rval * rval;
      }
      #pragma omp atomic
      global_mean += my_mean;
      #pragma omp atomic
      global_var += my_sqmean;
    }
    cse6230_mean += global_mean;
    cse6230_var  += global_var;
    global_mean  /= N;
    time = toc(&timer);
    if (i) {
      min_time[4] = time < min_time[4] ? time : min_time[4];
      max_time[4] = time > max_time[4] ? time : max_time[4];
      mean_time[4] += time;
    }

    timer = tic();
    global_mean = 0.;
    global_var = 0.;
    #pragma omp parallel
    {
      int            j, seed = 0;
      double         my_mean = 0.;
      double         my_sqmean = 0.;
      cse6230nrand_t nrand;
#if defined(_OPENMP)
      seed = omp_get_thread_num();
#endif
      cse6230nrand_seed(seed * NTIMES + i,&nrand);
      #pragma omp for
      for (j = 0; j < N; j++) {
        double rval = cse6230nrand(&nrand);
        my_mean += rval;
        my_sqmean += rval * rval;
      }
      #pragma omp atomic
      global_mean += my_mean;
      #pragma omp atomic
      global_var += my_sqmean;
    }
    cse6230n_mean += global_mean;
    cse6230n_var  += global_var;
    global_mean  /= N;
    time = toc(&timer);
    if (i) {
      min_time[5] = time < min_time[5] ? time : min_time[5];
      max_time[5] = time > max_time[5] ? time : max_time[5];
      mean_time[5] += time;
    }
  }
  for (i = 0; i < NPRNGS; i++) mean_time[i] /= (NTIMES - 1);
  printf ("Time for %d rand() calls: %f [%f, %f]\n",N,mean_time[0],min_time[0],max_time[0]);
  printf ("Time for %d rand_r() calls: %f [%f, %f]\n",N,mean_time[1],min_time[1],max_time[1]);
  printf ("Time for %d drand48() calls: %f [%f, %f]\n",N,mean_time[2],min_time[2],max_time[2]);
  printf ("Time for %d erand48() calls: %f [%f, %f]\n",N,mean_time[3],min_time[3],max_time[3]);
  printf ("Time for %d cse6240rand() calls: %f [%f, %f]\n",N,mean_time[4],min_time[4],max_time[4]);
  printf ("Time for %d cse6250nrand() calls: %f [%f, %f]\n",N,mean_time[5],min_time[5],max_time[5]);

  printf ("cse6240rand() rate: %e random GB/s\n",N * sizeof(double) / mean_time[4] * 1.e-9);
  cse6230_mean /= (N * NTIMES);
  cse6230_var  /= (N * NTIMES) - 1;
  cse6230_var  -= cse6230_mean * cse6230_mean;
  printf ("cse6240rand() mean %e var %e\n",cse6230_mean,cse6230_var);
  {
    double t = (cse6230_mean - 0.5) / sqrt(cse6230_var / N);
    double p = 0.5 * (erfc(-fabs(t) / sqrt(2)) - erfc(fabs(t) / sqrt(2)));

    printf ("t-test: %e, p-value ~ %e\n",t,p);
  }

  printf ("cse6240nrand() rate: %e random GB/s\n",N * sizeof(double) / mean_time[5] * 1.e-9);
  cse6230n_mean /= (N * NTIMES);
  cse6230n_var  /= (N * NTIMES) - 1;
  cse6230n_var  -= cse6230n_mean * cse6230n_mean;
  printf ("cse6240nrand() mean %e var %e\n",cse6230n_mean,cse6230n_var);
  {
    double t = cse6230n_mean / sqrt(cse6230n_var / N);
    double p = 0.5 * (erfc(-fabs(t) / sqrt(2)) - erfc(fabs(t) / sqrt(2)));

    printf ("t-test: %e, p-value ~ %e\n",t,p);
  }
  return 0;
}

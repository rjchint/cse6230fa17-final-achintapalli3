======================
Gauss-Seidel Iteration
======================

:author: CSE6230
:date:   Fall 2017

Definition
==========

We discretize a function :math:`u` on the 3D unit cube at :math:`N-1` interior
points in each direction, :math:`u_{i,j,k} := u(i/N,j/N,k/N)`.  We approximate
the Laplacian :math:`-\Delta` operator by

.. math::

    \begin{aligned}
      -\Delta u(i/N,j/N,k/N) \approx \phantom{-}&\frac{8}{3N} u_{i,j,k} \\
       -&\frac{1}{6N} (u_{i,j\pm 1,k\pm 1} + u_{i\pm 1, j,k \pm 1} + u_{i\pm 1, j\pm 1,k}) & [\text{12 edge neighbors}]\\
      -&\frac{1}{12N} (u_{i\pm 1,j\pm 1\,k \pm 1}) & [\text{8 corner neighbors}],
    \end{aligned}

subtituting 0 for missing neighbors at the boundaries.

The application of the approximate operator to every point is equivalent to a
matrix :math:`K\in\mathbb{R}^{(N-1)^3,(N-1)^3}`. and we wish to solve :math:`K
v = b`, where :math:`v` is a vector of the :math:`u_{i,j,k}` values and
:math:`b` is arbitrary.

The matrix :math:`K` is symmetric positive definite and can be written as
:math:`K = L + D + L^T`, where :math:`D` is the diagonal part and :math:`L` is
below the diagonal.

The *Gauss-Seidel iteration* to update an approximate solution :math:`v^i`
given :math:`b` is

.. math::

    \begin{aligned}
      v^{i+1} &\leftarrow v^i + (L + D)^{-1} (b - K v^i).
    \end{aligned}

Ordering
--------

The Gauss-Seidel sweep can be in any order, but performance is measured not by
iterations performed per second, but the rate of the reduction of the 2-norm of
the residual :math:`b - K v^{i-1}`.

=========================
Matrix-Matrix Multply-Add
=========================

:author: CSE 6230
:date:   Fall 2017

Definition
==========

Given matrices :math:`A \in \mathbb{R}^{M,N}`, :math:`B \in \mathbb{R}^{N,R}`,
and :math:`C \in \mathbb{R}^{M,R}`, compute the update
:math:`C \leftarrow \alpha C + A B`, that is

.. math::

    C_{i,j} \leftarrow \alpha C_{i,j} + \sum_{k=0}^{N-1} A_{i,k} B_{k,i}.

Precision
=========

This project may be completed in single precision.

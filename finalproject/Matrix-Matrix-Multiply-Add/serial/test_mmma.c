const char help[] = "Test driver for the correctness of matrix-matrix multiply-add implementation";

#include <petscviewer.h>
#include <petscblaslapack.h>
#include "mmma.h"

int main(int argc, char **argv)
{
  PetscInt       test, numTests = 1;
  PetscInt       scale = 20;
  PetscRandom    rand;
  MPI_Comm       comm;
  PetscViewer    viewer;
  PetscErrorCode ierr;

  ierr = PetscInitialize(&argc, &argv, NULL, help); if (ierr) return ierr;
  comm = PETSC_COMM_WORLD;
  viewer = PETSC_VIEWER_STDOUT_WORLD;
  ierr = PetscOptionsBegin(comm, NULL, "Matrix-Matrix Multiply-Add Test Options", "test_mmma.c");CHKERRQ(ierr);
  ierr = PetscOptionsInt("-num_tests", "Number of tests to run", "test_mmma.c", numTests, &numTests, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsInt("-scale", "Scale (log2) of the matrices in the test", "test_mmma.c", scale, &scale, NULL);CHKERRQ(ierr);
  ierr = PetscOptionsEnd();CHKERRQ(ierr);

  ierr = PetscRandomCreate(comm, &rand);CHKERRQ(ierr);
  ierr = PetscRandomSetFromOptions(rand);CHKERRQ(ierr);
  ierr = PetscRandomSeed(rand);CHKERRQ(ierr);

  ierr = PetscViewerASCIIPrintf(viewer, "Running %D tests of matrix_matrix_multiply_add()\n", numTests);CHKERRQ(ierr);
  ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
  for (test = 0; test < numTests; test++) {
    PetscScalar *Aa, *Ba, *Ca, *Da;
    PetscReal    logm, logn, logr, alpha, diff;
    PetscInt     m, n, r, i;

    ierr = PetscViewerASCIIPrintf(viewer, "Test %D:\n", test);CHKERRQ(ierr);
    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscRandomSetInterval(rand, 0, (PetscReal) scale);CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &logm);CHKERRQ(ierr);
    logn = scale - logm;

    ierr = PetscRandomSetInterval(rand, 0, PetscMin(logn, logm));CHKERRQ(ierr);
    ierr = PetscRandomGetValueReal(rand, &logr);CHKERRQ(ierr);

    m = (PetscInt) PetscPowReal(2., logm);
    n = (PetscInt) PetscPowReal(2., logn);
    r = (PetscInt) PetscPowReal(2., logr);

    ierr = PetscViewerASCIIPrintf(viewer, "Test dimensions: C = [%D x %D], A = [%D x %D], B = [%D x %D]\n", m, n, m, r, r, n);CHKERRQ(ierr);

    ierr = PetscMalloc4(m*n, &Ca, m*r, &Aa, r*n, &Ba, m*n, &Da);CHKERRQ(ierr);

    ierr = PetscRandomSetInterval(rand, -1., 1.);CHKERRQ(ierr);

    ierr = PetscRandomGetValue(rand, &alpha);CHKERRQ(ierr);

    for (i = 0; i < m * n; i++) {
      ierr = PetscRandomGetValue(rand, &Ca[i]);CHKERRQ(ierr);
      Da[i] = Ca[i];
    }
    for (i = 0; i < m * r; i++) {
      ierr = PetscRandomGetValue(rand, &Aa[i]);CHKERRQ(ierr);
    }
    for (i = 0; i < r * n; i++) {
      ierr = PetscRandomGetValue(rand, &Ba[i]);CHKERRQ(ierr);
    }

    ierr = matrix_matrix_multiply_add((size_t) m, (size_t) n, (size_t) r, alpha, Ca, Aa, Ba);CHKERRQ(ierr);


    {
      PetscBLASInt mb = m, nb = n, rb = r;
      PetscReal one = 1.;

      PetscStackCallBLAS("BLASgemm", BLASgemm_("N", "N", &nb, &mb, &rb, &one, Ba, &nb, Aa, &rb, &alpha, Da, &nb));
    }

    diff = 0.;
    for (i = 0; i < m *  n; i++) {
      PetscReal res = Da[i] - Ca[i];

      diff += PetscRealPart(res * PetscConj(res));
    }
    ierr = PetscFree4(Ca, Aa, Ba, Da);CHKERRQ(ierr);

    diff = PetscSqrtReal(diff);

    if (diff > PETSC_SMALL) SETERRQ3(comm, PETSC_ERR_LIB, "Test %D failed residual test at threshold %g with value %g\n", test, (double) PETSC_SMALL, (double) diff);

    ierr = PetscViewerASCIIPushTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscViewerASCIIPrintf(viewer, "Passed.\n");CHKERRQ(ierr);

    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
    ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  }
  ierr = PetscViewerASCIIPopTab(PETSC_VIEWER_STDOUT_WORLD);
  ierr = PetscRandomDestroy(&rand);CHKERRQ(ierr);

  ierr = PetscFinalize();
  return ierr;
}
